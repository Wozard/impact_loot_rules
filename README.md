# Impact Loot Rules

## Required Addon
We use [CEPGP](https://www.curseforge.com/wow/addons/cepgp) to distribute loot. Every guild member is required to install CEPGP to bid on loot.

## EP/GP

We use the EP/GP system for loot bidding. The system is [well-documented](http://www.epgpweb.com/help/system) online, so this document will cover Impact's specifics.

#### Bid Types
Our implementation supports two types of bids.
* __Full Price__: The bidder with the highest PR receives the item at full GP cost.
* __90% Discount Roll__: If there are no full price bids, then all discount bidders /roll, and the item is awarded to the roll winner for 10% of the GP cost.

#### Numbers
* __EP per MC__: 50
* __EP per BWL__: 70
* __EP per Onyxia__: 0
* __Weekly Decay (Tues)__: 10%
* __Minimum GP__: 200

## Class and Spec Priority
The following spreadsheets denote our rulings on the class/spec allocation priority of each item.

[Molten Core and Onyxia Spreadsheet](https://drive.google.com/file/d/1KfSH3qjfJ5tIB1wArEfVeq_qlXRUj5sb/view?usp=sharing)

[Blackwing Lair Spreadsheet](https://drive.google.com/file/d/1FCeuqbECbJQ86yQQxdIDWlAabOD1swDB/view?usp=sharing)

Items are allocated to the winning bidder with a class/spec in the First Priority column. If there are no first priority bids, then second priority bids are considered, and then all bids from those that can use the item.

Questions and feedback on specific item allocations are always welcome!

## Loot Counciled Items
The items listed here will be allocated via loot council.
* __Geddon Bindings of the Windseeker__: Beljuun
* __Garr Bindings of the Windseeker__: Wozard
* __Eye of Sulfuras__: Roick
* __Crafting Drops__: Guild Bank/Requisite Crafters

## The Bench
Players that are benched due to a full raid, but who are otherwise on-time and available, receive full points for the raiding night.

Benched players are responsible for recording their benched status in the **#attendance_chat** channel on the raid night.
